package com.nexos.rest.RestCalculadora.Controlador;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.nexos.rest.RestCalculadora.Soap.SoapCliente;
import com.nexos.rest.RestCalculadora.exception.*;
import com.nexos.rest.RestCalculadora.modelo.TipoOperacion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * Rest encargado de servir las solicitudes
 */
@RestController
@RequestMapping("api1")
public class ControladorRest {
    Logger logger = LoggerFactory.getLogger(ControladorRest.class);


    /*
        Calcular Pendiente
* */
    @PostMapping(value = "/pendiente", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    public TipoOperacion calcularPendiente(@Validated @RequestBody TipoOperacion tipoOperacion) throws JsonProcessingException, ResourceNotFoundException {
        logger.info("Ingreso al método calcularPendiente");
        SoapCliente calcularOperacionesPendiente = new SoapCliente();
        TipoOperacion valor = calcularOperacionesPendiente.calcularPendiente(tipoOperacion.getPrimerValor(),
                tipoOperacion.getSegundoValor(), tipoOperacion.getTercerValor(), tipoOperacion.getCuartoValor());
        logger.info("Finalizo al método calcularPendiente");
        return valor;

    }

    /*
    calcularPromedio
* */
    @PostMapping(value = "/promedio", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    public TipoOperacion calcularPromedio(@Validated @RequestBody TipoOperacion tipoOperacion) throws JsonProcessingException, ResourceNotFoundException {
        logger.info("Ingreso al método calcularPromedio");
        SoapCliente calcularOperacionesPendiente = new SoapCliente();
        TipoOperacion valor = calcularOperacionesPendiente.calcularPromedio(tipoOperacion.getPrimerValor(),
                tipoOperacion.getSegundoValor(), tipoOperacion.getTercerValor(), tipoOperacion.getCuartoValor());
        logger.info("Finalizo al método calcularPromedio");
        return valor;

    }


    /*
    calcular área de un triangulo
* */
    @PostMapping(value = "/areaTriangulo", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    public TipoOperacion calcularAreaTriangulo(@Validated @RequestBody TipoOperacion tipoOperacion) throws JsonProcessingException, ResourceNotFoundException {
        logger.info("Ingreso al método calcularAreaTriangulo");
        SoapCliente calcularOperaciones = new SoapCliente();
        TipoOperacion valor = calcularOperaciones.calcularAreaTriangulo( tipoOperacion.getPrimerValor(),
                tipoOperacion.getSegundoValor());
        logger.info("Finalizo al método calcularAreaTriangulo");
        return valor;

    }

    /*
calcular área de un Circulo
* */
    @PostMapping(value = "/areaCirculo", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    public TipoOperacion calcularAreaCirculo(@Validated @RequestBody TipoOperacion tipoOperacion) throws JsonProcessingException, ResourceNotFoundException {
        logger.info("Ingreso al método calcularAreaCirculo");
        SoapCliente calcularOperaciones = new SoapCliente();
        TipoOperacion valor = calcularOperaciones.calcularAreaCirculo(tipoOperacion.getPrimerValor(),
                tipoOperacion.getSegundoValor());
        logger.info("Finalizo al método calcularAreaCirculo");
        return valor;

    }


}
