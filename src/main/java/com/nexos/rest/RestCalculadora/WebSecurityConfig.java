package com.nexos.rest.RestCalculadora;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * https://github.com/woniper/spring-boot-restful-example/blob/master/src/main/java/net/woniper/spring/boot/restful/example/security/WebSecurityConfig.java
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {



    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        //Request pendiente
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api1/pendiente").permitAll().and().authorizeRequests().and().httpBasic();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api1/promedio").permitAll().and().authorizeRequests().and().httpBasic();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api1/areaTriangulo").permitAll().and().authorizeRequests().and().httpBasic();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api1/areaCirculo").permitAll().and().authorizeRequests().and().httpBasic();

    }
}
